# Loopy Genome Sequencer project
Development of the loopy genome sequencer project

## The rough concept
The idea is that the project expressively sonifies (or: sequences) a "genomic code" as a drum rhythm. 
This rhythm is constantly repeated as a loop. However, it does not remain identical because it is subject to  mutation. With every repetition of the loop, there is a chance that a mutation occurs that changes the rhythm. 
The kind of mutation happens by pure chance. But the likelihood of occurrence of a mutation is also determined by external parameters that can be influenced from the outside by interacting with the contraption. 

## Materials

- 4 solenoids
- 4 watering cans or maybe better objects
- 1 sensor for light or temperature, optionally 2
- "bRatAmp" consisting of one Arduino Nano, 6 MOSFET modules and a 12 V external power supply 

## The concept in practice/practical implementation
There is a grid, lets say of 4 channels (4 amino acids) and one 4/4 bar with 16th notes. So the grid consists of 16 positions per channel. This grid is the „genome“. 

The program starts with one pattern and this pattern is played back in a loop: the notes trigger the solenoid drums. 
From time to time, there is a „mutation“. When this happens, the position of an onset is shifted in position. 
As a result there is a new rhythm.

### Interacting with the contraption
When the light is brighter (or temperature higher) there are more mutations. Optionally it could also react to "human presence" although I don't know where to buy such a sensor ;)

Other parameters might be possible:
Another variable could increase the speed with which the pattern is played back.
 

### Other things 
- There should be one solenoid that stays regular so that one does not lose track of the pattern
- There could also be a randomization process based on the intensity of the beat

## Functionality
Everything is coded on the Arduino.
There is actually a library for random processes:
https://www.arduino.cc/reference/en/language/functions/random-numbers/random/


## To do list
- improve concept
- think about visual style



